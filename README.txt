
A tiny module that styles the 2.x branch of IMCE to match the Kama theme of CKEditor. It provides a more unified experience for content managers.
